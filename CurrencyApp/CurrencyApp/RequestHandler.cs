﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace CurrencyApp
{
    class RequestHandler
    {
        WebClient client = new WebClient();
        public string PickedDate { get; set; }
        public string SendRequest()
        {
            string url = "http://apilayer.net/api/historical?access_key=9c29857464e1abf3c3a65c8fabc0b0df&date=" + PickedDate;
            string json = client.DownloadString(url);
            return json;
        }
    }
}


