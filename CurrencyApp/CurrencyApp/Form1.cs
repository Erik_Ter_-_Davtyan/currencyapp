﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace CurrencyApp
{
    public partial class Form1 : Form
    {
        RequestHandler rh = new RequestHandler();
        public Form1()
        {
            InitializeComponent();
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            rh.PickedDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            string json = rh.SendRequest();
            Quotes crs = JsonConvert.DeserializeObject<Quotes>(json);

        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
